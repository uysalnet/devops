Import-Module ActiveDirectory
$user = Import-Csv c:\user.csv
$user | ForEach-Object{
New-ADUser -Name $_.Name -UserPrincipalName $_.UserPrincipalName -SamAccountName $_.Username -GivenName $_.GivenName -DisplayName $_.DisplayName -SurName $_.Surname -AccountPassword (ConvertTo-SecureString inf*stone -AsPlainText -force) -Path $_.Path -Enabled $true
}