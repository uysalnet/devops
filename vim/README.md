- install latest vim 8 from source

 git clone https://github.com/vim/vim.git
 cd vim
 # make distclean  
 # if you build Vim before
 make -j8
 sudo make install
 cp src/vim /usr/bin
