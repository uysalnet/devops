#!/bin/bash
if ! rpm -qa | grep -q vim; then
yum install vim -y
elif ! rpm -qa | grep -q git; then
yum install git -y
fi
# configure vim plugins
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
curl -fLo ~/.vimrc https://gitlab.com/uysalnet/devops/raw/master/vim/vimrc
vim +PlugInstall +qall
